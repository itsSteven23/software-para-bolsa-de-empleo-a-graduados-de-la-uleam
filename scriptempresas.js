document.addEventListener('DOMContentLoaded', function() {
    // Obtener el contenedor de empresas colaboradoras
    const empresasColaboradorasDiv = document.getElementById('empresas-colaboradoras');

    // Simulación de empresas colaboradoras (en un entorno real, esto vendría del servidor)
    const empresasColaboradorasSimuladas = [
        { nombre: 'Empresa A', sector: 'Tecnología', ubicacion: 'Quito' },
        { nombre: 'Empresa B', sector: 'Salud', ubicacion: 'Guayaquil' },
        // Agrega más empresas colaboradoras simuladas según sea necesario
    ];

    // Mostrar las empresas colaboradoras simuladas en la sección
    mostrarEmpresasColaboradoras(empresasColaboradorasSimuladas, empresasColaboradorasDiv);
});

// Función para mostrar las empresas colaboradoras en la sección
function mostrarEmpresasColaboradoras(empresasColaboradoras, contenedor) {
    // Limpiar el contenido existente
    contenedor.innerHTML = '';

    // Verificar si hay empresas colaboradoras
    if (empresasColaboradoras.length === 0) {
        contenedor.innerHTML = '<p>No hay empresas colaboradoras registradas en este momento.</p>';
        return;
    }

    // Crear una lista de empresas colaboradoras
    const listaEmpresasColaboradoras = document.createElement('ul');
    empresasColaboradoras.forEach(function (empresa) {
        const item = document.createElement('li');
        item.innerHTML = `<strong>${empresa.nombre}</strong> - Sector: ${empresa.sector}, Ubicación: ${empresa.ubicacion}`;
        listaEmpresasColaboradoras.appendChild(item);
    });

    // Agregar la lista a la sección de empresas colaboradoras
    contenedor.appendChild(listaEmpresasColaboradoras);
}

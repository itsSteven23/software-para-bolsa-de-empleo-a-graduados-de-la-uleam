document.addEventListener('DOMContentLoaded', function() {
    const busquedaForm = document.getElementById('busqueda-form');
    const contenidoSection = document.getElementById('contenido');

    busquedaForm.addEventListener('submit', function(event) {
        event.preventDefault(); // Evitar que el formulario se envíe automáticamente

        // Obtener valores del formulario
        const titulo = document.getElementById('titulo').value;
        const empresa = document.getElementById('empresa').value;
        const ubicacion = document.getElementById('ubicacion').value;

        // Simulación de búsqueda (en un entorno real, esto se haría en el servidor)
        const resultadosSimulados = [
            { titulo: 'Desarrollador Web', empresa: 'Empresa A', ubicacion: 'Quito' },
            { titulo: 'Ingeniero de Software', empresa: 'Empresa B', ubicacion: 'Guayaquil' },
            // Agrega más resultados simulados según sea necesario
        ];

        // Mostrar los resultados simulados en la sección de contenido
        mostrarResultados(resultadosSimulados);
    });

    // Función para mostrar los resultados en la sección de contenido
    function mostrarResultados(resultados) {
        // Limpiar el contenido existente
        contenidoSection.innerHTML = '';

        // Verificar si hay resultados
        if (resultados.length === 0) {
            contenidoSection.innerHTML = '<p>No se encontraron resultados.</p>';
            return;
        }

        // Crear una lista de resultados
        const listaResultados = document.createElement('ul');
        resultados.forEach(function (empleo) {
            const item = document.createElement('li');
            item.innerHTML = `<strong>${empleo.titulo}</strong> en ${empleo.empresa}, ubicado en ${empleo.ubicacion}.`;
            listaResultados.appendChild(item);
        });

        // Agregar la lista a la sección de contenido
        contenidoSection.appendChild(listaResultados);
    }
});

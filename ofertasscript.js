document.addEventListener('DOMContentLoaded', function() {
    // Obtener el contenedor de ofertas exclusivas
    const ofertasExclusivasDiv = document.getElementById('ofertas-exclusivas');

    // Simulación de ofertas exclusivas (en un entorno real, esto vendría del servidor)
    const ofertasExclusivasSimuladas = [
        { puesto: 'Desarrollador Frontend', empresa: 'Empresa C', ubicacion: 'Cuenca' },
        { puesto: 'Diseñador UX/UI', empresa: 'Empresa D', ubicacion: 'Manta' },
        // Agrega más ofertas exclusivas simuladas según sea necesario
    ];

    // Mostrar las ofertas exclusivas simuladas en la sección
    mostrarOfertasExclusivas(ofertasExclusivasSimuladas, ofertasExclusivasDiv);
});

// Función para mostrar las ofertas exclusivas en la sección
function mostrarOfertasExclusivas(ofertasExclusivas, contenedor) {
    // Limpiar el contenido existente
    contenedor.innerHTML = '';

    // Verificar si hay ofertas exclusivas
    if (ofertasExclusivas.length === 0) {
        contenedor.innerHTML = '<p>No hay ofertas exclusivas disponibles en este momento.</p>';
        return;
    }

    // Crear una lista de ofertas exclusivas
    const listaOfertasExclusivas = document.createElement('ul');
    ofertasExclusivas.forEach(function (oferta) {
        const item = document.createElement('li');
        item.innerHTML = `<strong>${oferta.puesto}</strong> en ${oferta.empresa}, Ubicación: ${oferta.ubicacion}`;
        listaOfertasExclusivas.appendChild(item);
    });

    // Agregar la lista a la sección de ofertas exclusivas
    contenedor.appendChild(listaOfertasExclusivas);
}

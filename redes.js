document.addEventListener('DOMContentLoaded', function() {
    // Obtener el contenedor de redes sociales y contacto
    const redesYContactoDiv = document.getElementById('redes-y-contacto');

    // Simulación de enlaces a redes sociales y contacto (en un entorno real, esto vendría del servidor)
    const redesYContactoSimulados = [
        { tipo: 'Facebook', enlace: 'https://www.facebook.com/bolsaempleouleam/' },
        { tipo: 'Twitter', enlace: 'https://twitter.com/bolsaempleouleam' },
        { tipo: 'LinkedIn', enlace: 'https://www.linkedin.com/company/bolsaempleouleam' },
        { tipo: 'Correo Electrónico', enlace: 'mailto:info@bolsaempleouleam.com' }
        // Agrega más enlaces simulados según sea necesario
    ];

    // Mostrar los enlaces simulados en la sección
    mostrarRedesYContacto(redesYContactoSimulados, redesYContactoDiv);
});

// Función para mostrar los enlaces a redes sociales y la información de contacto en la sección
function mostrarRedesYContacto(redesYContacto, contenedor) {
    // Limpiar el contenido existente
    contenedor.innerHTML = '';

    // Verificar si hay enlaces y contactos
    if (redesYContacto.length === 0) {
        contenedor.innerHTML = '<p>No hay información disponible en este momento.</p>';
        return;
    }

    // Crear una lista de enlaces y contactos
    const listaRedesYContacto = document.createElement('ul');
    redesYContacto.forEach(function (item) {
        const listItem = document.createElement('li');
        if (item.tipo === 'Correo Electrónico') {
            listItem.innerHTML = `<a href="${item.enlace}">${item.tipo}</a>`;
        } else {
            listItem.innerHTML = `<a href="${item.enlace}" target="_blank">${item.tipo}</a>`;
        }
        listaRedesYContacto.appendChild(listItem);
    });

    // Agregar la lista a la sección de redes sociales y contacto
    contenedor.appendChild(listaRedesYContacto);
}

document.addEventListener('DOMContentLoaded', function() {
    // Obtener el contenedor de eventos y talleres
    const eventosTalleresDiv = document.getElementById('eventos-talleres');

    // Simulación de eventos y talleres (en un entorno real, esto vendría del servidor)
    const eventosTalleresSimulados = [
        { nombre: 'Taller de Desarrollo Profesional', fecha: '2023-11-01', ubicacion: 'Salón 101' },
        { nombre: 'Conferencia sobre Tecnologías Emergentes', fecha: '2023-11-15', ubicacion: 'Auditorio Principal' },
        // Agrega más eventos y talleres simulados según sea necesario
    ];

    // Mostrar los eventos y talleres simulados en la sección
    mostrarEventosTalleres(eventosTalleresSimulados, eventosTalleresDiv);
});

// Función para mostrar los eventos y talleres en la sección
function mostrarEventosTalleres(eventosTalleres, contenedor) {
    // Limpiar el contenido existente
    contenedor.innerHTML = '';

    // Verificar si hay eventos y talleres
    if (eventosTalleres.length === 0) {
        contenedor.innerHTML = '<p>No hay eventos ni talleres programados en este momento.</p>';
        return;
    }

    // Crear una lista de eventos y talleres
    const listaEventosTalleres = document.createElement('ul');
    eventosTalleres.forEach(function (evento) {
        const item = document.createElement('li');
        item.innerHTML = `<strong>${evento.nombre}</strong> - Fecha: ${evento.fecha}, Ubicación: ${evento.ubicacion}`;
        listaEventosTalleres.appendChild(item);
    });

    // Agregar la lista a la sección de eventos y talleres
    contenedor.appendChild(listaEventosTalleres);
}

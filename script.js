document.addEventListener('DOMContentLoaded', function() {
    const loginForm = document.getElementById('login-form');

    loginForm.addEventListener('submit', function(event) {
        event.preventDefault(); // Evitar que el formulario se envíe automáticamente

        // Obtener valores del formulario
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;

        // Validar las credenciales (esto debería hacerse en el servidor en un entorno de producción)
        if (email === 'usuario@example.com' && password === 'contraseña') {
            alert('Inicio de sesión exitoso');
            // Redirigir al usuario a la página principal
            window.location.href = 'paginainicio.html';
        } else {
            alert('Credenciales incorrectas. Intenta de nuevo.');
        }
    });
});
